import React, {Component} from 'react';
import './App.css';
import {List} from './models/List';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Lists from './containers/Lists/Lists';
import ListDetail from './containers/ListDetail/ListDetail';
import Reminders from './containers/Reminders/Reminders';
import {ListController} from './controllers/ListController';
import {ReminderDetail} from './containers/ReminderDetail/ReminderDetail';
import {IReminder} from './models/Reminder';

/**
 * Definisco tutte le proprietà (minime e massime) che il componente <App /> può avere
 */
interface AppProps {
}

interface AppState {
	elenchi: List[];
}

/**
 * Componente classe
 */
export default class App extends Component<AppProps, AppState> {
	listController: ListController = new ListController();

	/**
	 * Costruisco l'istanza della classe App, il costruttore accetta
	 * un solo parametro `props` che è un OGGETTO di tipo `AppProps`.
	 *
	 * Sono obbligato a chiamare il `super` che è il costruttore del componente `Component`
	 * che la classe `App` estende, il super accetta un solo parametro `props` che
	 * è un OGGETTO di tipo `AppProps` ovvero quello che ho passato come
	 * parametro al costruttore di `App`.
	 *
	 * Il `super(props)` aggiunge come ATTRIBUTO della classe `App` il
	 * parametro `props` in modo da poter accedere ai suoi attributi
	 * negli altri metodi della classe utilizzando `this.props.nomeAttributo`
	 * ex. this.props.title
	 *
	 * dopo `super` devo inizializzare lo state (se previsto)
	 */
	constructor(props: AppProps) {
		super(props);
		this.state = {
			elenchi: [],
		};
	}

	/**
	 * viene chiamato una sola volta da React quando il componente è stato costruito ed è pronto per essere utilizzato
	 * faccio OVERRIDING del metodo della classe
	 */
	componentDidMount() {
		this.fetchLists();
	}

	fetchLists() {
		this.listController.getLists()
			.then(lists => {
				this.setState({elenchi: lists});
			})
			.catch(error => {
				console.error('Impossibile ottenere la lista!', error);
			})
	}

	/**
	 * Richiamata quando l'utente vuole contrassegnare un promemoria con fatto/da fare
	 */
	onDoneToggle(index: number, idList: string) {
		const list = this.state.elenchi.find(l => l.id === idList);
		if (list) {
			list.reminders[index].done = !list.reminders[index].done;
			this.listController.updateList(list)
				.then(() => this.fetchLists())
				.catch((error) => console.error(error));
		}
	}

	/**
	 * Richiamato per creare o aggiornare un promemoria
	 */
	onSaveReminder(data: IReminder, list: List, history: any, index?: number) {
		/**
		 * dichiaro onSuccess in modo da riutilizzarla
		 */
		const onSuccess = () => {
			this.fetchLists();
			history.push('/lists/' + list.id + '/reminders');
		};
		/**
		 * dichiaro onError in modo da riutilizzarla
		 */
		const onError = (error: any) => console.error(error);

		if (index === undefined) {
			this.listController.addReminder(data, list).then(onSuccess).catch(onError);
		} else {
			this.listController.updateReminder(data, index, list).then(onSuccess).catch(onError);
		}

	}

	/**
	 * Richiamato per eliminare un promemoria
	 */
	onDeleteReminder(list: List, history: any, index: number) {
		this.listController.removeReminder(list, index)
			.then(() => {
				this.fetchLists();
				history.push('/lists/' + list.id + '/reminders');
			})
			.catch((error) => console.error(error));
	}

	/**
	 * Questo metodo `render` ha il compito di ritornare l' "HTML" da mostrare a video
	 * quando viene utilizzato.
	 *
	 * Devo ritornare PER FORZA un solo elemento JSX che contiene tutti gli altri!
	 */
	render() {
		/**
		 * 1. Inseriamo URL nel browser
		 * 2. Il componente `BrowserRouter` legge l'url
		 * 3. Il `BrowserRouter` applica lo switch del path dell'url
		 * 4. Sceglie la `Route` che ha il path che matcha con quello dell'url
		 * 5. chiama la funzione `render` della Route scelta
		 * 6. la funzione `render` ritornerà il container da renderizzare
		 */
		return <BrowserRouter>
			<Switch>
				<Route
					path="/lists/:listId/reminders/:index"
					render={(props) => {
						const isNew: boolean = props.match.params.index === 'new';
						const matchedList = this.state.elenchi.find(l => l.id === props.match.params.listId);
						const matchedReminder = matchedList?.reminders[+props.match.params.index];
						return <ReminderDetail
							isNew={isNew}
							list={matchedList}
							reminder={matchedReminder}
							onSave={(data: IReminder) => {
								if (matchedList) {
									this.onSaveReminder(
										data,
										matchedList,
										props.history,
										!isNew ? +props.match.params.index : undefined
									)
								}
							}}
							onDelete={() => {
								if (matchedList) {
									this.onDeleteReminder(
										matchedList,
										props.history,
										+props.match.params.index
									)
								}
							}}
							{...props}
						/>
					}}
				/>
				<Route
					path="/lists/:id/reminders"
					render={(props) => {
						const idElenco: string = props.match.params.id;
						const elencoTrovato = this.state.elenchi.find((elenco) => elenco.id === idElenco);
						return <Reminders
							elenco={elencoTrovato}
							reminders={(elencoTrovato) ? elencoTrovato.reminders : undefined}
							onDoneToggle={(index: number, idList: string) => this.onDoneToggle(index, idList)}
							{...props}
						/>
					}}
				/>
				<Route
					path="/lists/:id"
					render={(routingProps) => {
						/**
						 * ottengo l'id passato nella rotta
						 */
						const idElenco: string = routingProps.match.params.id;
						const isNew: boolean = idElenco === 'new';
						console.log('id', idElenco);
						/**
						 * Cerco all'interno dell'array l'elemento che ha id === idElenco
						 * se lo trova lo restituisce, altrimenti ritorna undefined
						 */
						const elenco = this.state.elenchi.find((elenco) => elenco.id === idElenco);
						return <ListDetail
							isNew={isNew}
							elenco={elenco}
							onCreate={(name, color) => {
								/**
								 * creo un nuovo elenco nel database tramite il server
								 */
								this.listController.createList(name, color)
									.then(list => {
										/**
										 * riottengo la lista aggiornata dal server, che quindi conterrà
										 * anche il nuovo elenco creato
										 */
										this.listController.getLists()
											.then(elenchi => {
												/**
												 * aggiorno la lista dello state
												 */
												this.setState({elenchi: elenchi});
												/**
												 * Navigo nella pagina degli elenchi
												 */
												routingProps.history.push('/');
											})
											.catch(error => console.error('Impossibile ottenere la lista di elenchi'))
									})
									.catch(error => console.error('Impossibile creare Elenco', error));
							}}
							onUpdate={(name, color, id) => {
								const elenco: List | undefined = this.state.elenchi.find(list => list.id === id);
								if (elenco) {
									elenco.name = name;
									elenco.color = color;
									this.listController.updateList(elenco)
										.then(list => {
											this.listController.getLists()
												.then(elenchi => {
													this.setState({elenchi: elenchi});
													routingProps.history.push('/');
												})
												.catch(error => console.error('Impossibile ottenere la lista di elenchi'))
										})
										.catch(error => console.error("Impossibile aggiornare l'elenco"));
								} else {
									console.error('Elenco non trovato');
								}
							}}
							onDelete={(id) => {
								this.listController.deleteList(id)
									.then(() => {
										this.listController.getLists()
											.then(elenchi => {
												this.setState({elenchi: elenchi});
												routingProps.history.push('/');
											})
											.catch(error => console.error('Impossibile ottenere la lista di elenchi'))
									})
									.catch(error => console.error("Impossibile eliminare l'elenco"));
							}}
							{...routingProps}
						/>
					}}
				/>
				<Route
					path="/"
					render={(props) => <Lists elenchi={this.state.elenchi} {...props} />}
				/>
			</Switch>
		</BrowserRouter>
	}
}
