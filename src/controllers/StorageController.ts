export class StorageController {
	/**
	 * Salva un dato nel localStorage
	 */
	setInLocal(key: string, value: any): void {
		localStorage.setItem(key, JSON.stringify(value));
	}

	/**
	 * Ottieni un dato dal localStorage
	 */
	getFromLocal(key: string): any {
		const item: string | null = localStorage.getItem(key);
		if (item) {
			return JSON.parse(item);
		}
		return null;
	}

	/**
	 * Elimina uno specifico valore dal localStorage
	 */
	deleteFromLocal(key: string) {
		localStorage.removeItem(key);
	}

	/**
	 * svuota tutti i dati contenuti nel localStorage
	 */
	clearLocal() {
		localStorage.clear();
	}

	/**
	 * Salva un dato nel sessionStorage
	 */
	setInSession(key: string, value: any): void {
		sessionStorage.setItem(key, JSON.stringify(value));
	}

	/**
	 * Ottieni un dato dal sessionStorage
	 */
	getFromSession(key: string): any {
		const item: string | null = sessionStorage.getItem(key);
		if (item) {
			return JSON.parse(item);
		}
		return null;
	}

	/**
	 * Elimina uno specifico valore dal sessionStorage
	 */
	deleteFromSession(key: string) {
		sessionStorage.removeItem(key);
	}

	/**
	 * svuota tutti i dati contenuti nel sessionStorage
	 */
	clearSession() {
		sessionStorage.clear();
	}
}
