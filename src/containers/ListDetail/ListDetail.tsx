import {List} from '../../models/List';
import React, {Component, FormEvent} from 'react';
import Header from '../../components/Header/Header';
import {Color} from '../../models/Color.enum';
import {HeaderAction} from '../../components/Header/HeaderAction.enum';

interface ListDetailProps {
	elenco?: List;
	isNew: boolean;
	/**
	 * callback, quì si tipizza la funzione
	 * in questo caso la funzione accetta due parametri (name e color) e ritorna void, ovvero undefined (no return)
	 */
	onCreate: (name: string, color: Color) => void;
	onUpdate: (name: string, color: Color, id: string) => void;
	onDelete: (id: string) => void;
}

interface ListDetailState {
	name: string;
	color: Color;
	colors: Color[];
}

export default class ListDetail extends Component<ListDetailProps, ListDetailState> {
	constructor(props: ListDetailProps) {
		super(props);
		const colors: Color[] = Object.values(Color);
		this.state = {
			name: (props.elenco) ? props.elenco.name : '',
			color: (props.elenco) ? props.elenco.color : colors[0],
			colors: colors,
		};
	}

	/**
	 * verrà chiamato quando l'utente avrà cliccato su salva
	 */
	onSave(e: FormEvent): void {
		/**
		 * serve per non fare ricaricare la pagina al submit
		 */
		e.preventDefault();
		if (this.props.isNew) {
			this.props.onCreate(this.state.name, this.state.color);
		} else if (this.props.elenco) {
			this.props.onUpdate(this.state.name, this.state.color, this.props.elenco.id);
		} else {
			console.error('Elenco non presente!');
		}
	}

	render() {

		const prev = (this.props.elenco && !this.props.isNew) ? {
			title: this.props.elenco.name,
			link: '/lists/' + this.props.elenco.id + '/reminders',
		} : {
			title: 'Elenchi',
			link: '/',
		};

		return <>
			<Header
				title={(this.props.isNew) ? 'Nuovo elenco' : 'Modifica elenco'}
				prev={prev}
				actions={this.props.elenco && !this.props.isNew ? [HeaderAction.Delete] : undefined}
				onActionClick={(action) => this.props.elenco && this.props.onDelete(this.props.elenco.id)}
			/>
			<form onSubmit={(e: FormEvent) => this.onSave(e)}>
				<div className="container list-detail-container">
					<div className="form-input">
						<label>Nome elenco</label>
						<input type="text" value={this.state.name} onChange={(event) => this.setState({name: event.target.value})}/>
					</div>
					<div className="form-input">
						<label>Colore</label>
						<div className="colors">
							{this.state.colors.map((c) => <div
								key={c}
								className={"color" + (this.state.color === c ? ' active' : '')}
								style={{backgroundColor: c}}
								onClick={(event) => {
									console.log(1, this.state.color);
									/**
									 * Il setState è asincrono, esso chiama la callback dopo aver aggiornato lo state
									 */
									this.setState({color: c}, () => {
										console.log(2, this.state.color);
									});
								}}
							>
							</div>)}
						</div>
					</div>
				</div>
				<div className="footer">
					<button
						disabled={this.state.name.trim().length === 0}
						className="btn btn-primary"
						type="submit"
					>Salva
					</button>
				</div>
			</form>
		</>;
	}
}
