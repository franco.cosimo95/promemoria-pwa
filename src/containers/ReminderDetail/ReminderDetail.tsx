import React, {Component} from "react";
import {List} from "../../models/List";
import {IReminder, Reminder} from "../../models/Reminder";
import {HeaderAction} from "../../components/Header/HeaderAction.enum";
import Header from '../../components/Header/Header';

interface ReminderDetailProps {
	isNew: boolean;
	list?: List;
	reminder?: Reminder;
	onSave: (data: IReminder) => void;
	onDelete: () => void;
}

interface ReminderDetailState {
	name: string;
	description: string;
	date: string;
	position: string;
}

export class ReminderDetail extends Component<ReminderDetailProps, ReminderDetailState> {

	constructor(props: ReminderDetailProps) {
		super(props);
		this.state = {
			name: this.props.isNew ? '' : (this.props.reminder?.name || ''),
			description: this.props.isNew ? '' : (this.props.reminder?.description || ''),
			date: this.props.isNew ? '' : (this.props.reminder?.date || ''),
			position: this.props.isNew ? '' : (this.props.reminder?.position || ''),
		};
	}

	handleChange(key: string, event: any) {
		this.setState({[key]: (event.target.value)} as any);
	}

	handleSubmit(event: any) {
		event.preventDefault();
		this.props.onSave({
			name: this.state.name,
			description: this.state.description,
			date: this.state.date,
			position: this.state.position,
		});
	}

	onActionClick(action: HeaderAction) {
		if (action === HeaderAction.Delete) {
			this.props.onDelete();
		}
	}

	render() {
		const actions: HeaderAction[] = [];
		if (!this.props.isNew) {
			actions.push(HeaderAction.Delete);
		}
		return <>
			<Header
				title={this.props.isNew ? 'Nuovo promemoria' : (this.props.reminder?.name || 'Modifica promemoria')}
				actions={actions}
				prev={{
					title: this.props.list?.name || '',
					link: '/lists/' + this.props.list?.id + '/reminders',
				}}
				onActionClick={(action) => this.onActionClick(action)}
			/>
			<form onSubmit={(event) => this.handleSubmit(event)}>
				<div className="container list-detail-container">
					<div className="form-input">
						<label>Nome promemoria</label>
						<input type="text" value={this.state.name} onChange={(event) => this.handleChange('name', event)}/>
					</div>
					<div className="form-input">
						<label>Descrizione</label>
						<input type="text" value={this.state.description}
							   onChange={(event) => this.handleChange('description', event)}/>
					</div>
					<div className="form-input">
						<label>Data</label>
						<input type="date" value={this.state.date} onChange={(event) => this.handleChange('date', event)}/>
					</div>
					<div className="form-input">
						<label>Posizione</label>
						<input type="text" value={this.state.position} onChange={(event) => this.handleChange('position', event)}/>
					</div>
				</div>
				<div className="footer">
					<input
						disabled={!this.state.name.trim().length}
						className="btn btn-primary"
						type="submit"
						value="Salva"/>
				</div>
			</form>
		</>
	}
}
