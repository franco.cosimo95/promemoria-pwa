import React, {Component} from 'react';
import {List} from '../../models/List';
import Header from '../../components/Header/Header';
import {Link, RouteComponentProps} from 'react-router-dom';
import {HeaderAction} from '../../components/Header/HeaderAction.enum';
import {StorageController} from '../../controllers/StorageController';

interface ListsProps extends RouteComponentProps<any> {
	elenchi: List[];
}

export default class Lists extends Component<ListsProps> {
	storageController: StorageController = new StorageController();

	componentDidMount() {
		const key: string = 'test' + Date.now();
		this.storageController.setInSession(key, {prova: 12});
		console.log(this.storageController.getFromSession(key));
		this.storageController.setInLocal('testLocal', {prova: 12});
		console.log(this.storageController.getFromLocal('testLocal'));
		this.storageController.deleteFromLocal('MENTION_APP_SESSION')
		this.storageController.clearLocal()

		this.storageController.deleteFromSession('test1594721553441');
		this.storageController.clearSession();
	}

	render() {
		return <>
			<Header
				title="Elenchi"
				actions={[HeaderAction.Add]}
				onActionClick={(clickedAction) => this.props.history.push('/lists/new')}
			/>
			<div className="container lists-container">
				{this.props.elenchi.map((l, indice) => {
					return <Link className="list" to={'/lists/' + l.id + '/reminders'} key={l.id}>
						<div className="list-color" style={{backgroundColor: l.color}}/>
						<div className="list-body">
							<span className="list-name">
							  {l.name}
							</span>
							<span className="list-count">
							  {l.reminders.length}
							</span>
						</div>
					</Link>
				})}
			</div>
		</>
	}

}
