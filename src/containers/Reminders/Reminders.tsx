import React from 'react';
import {Reminder} from '../../models/Reminder';
import {RouteComponentProps} from 'react-router-dom';
import {List} from '../../models/List';
import Header from '../../components/Header/Header';
import {HeaderAction} from '../../components/Header/HeaderAction.enum';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronRight} from '@fortawesome/free-solid-svg-icons';

interface RemindersProps extends RouteComponentProps<any> {
	reminders?: Reminder[];
	elenco?: List;
	onDoneToggle: (index: number, idList: string) => void;
}

export default function Reminders(props: RemindersProps) {
	const onActionClick = (action: HeaderAction) => {
		switch (action) {
			case HeaderAction.Settings:
				props.history.push('/lists/' + props.elenco?.id);
				break;
			case HeaderAction.Add:
				props.history.push('/lists/' + props.elenco?.id + '/reminders/new');
				break;
		}
	};

	const onReminderClick = (index: number) => props.history.push('/lists/' + props.elenco?.id + '/reminders/' + index)

	return <>
		<Header
			title={props.elenco ? props.elenco.name : 'Promemoria'}
			prev={{
				title: 'Elenchi',
				link: '/',
			}}
			actions={[HeaderAction.Settings, HeaderAction.Add]}
			onActionClick={onActionClick}
		/>
		<div className="reminders-container">
		{(!props.reminders || props.reminders.length === 0)
			? <div className="no-reminder">Questo elenco non contiene promemoria</div>
			: props.reminders.map((r: Reminder, index) => (
				<div className="reminder" key={index} onClick={() => onReminderClick(index)}>
					<div
						className={"reminder-check"}
						style={r.done ? {backgroundColor: props.elenco?.color, borderColor: props.elenco?.color} : {}}
						onClick={(event) => {
							event.stopPropagation();
							props.elenco && props.onDoneToggle(index, props.elenco.id);
						}}
					/>
					<div className="reminder-body">
					  	<span className="reminder-name">
					  	{r.name}
						</span>
						<span className="reminder-desc">
							{r.description}
						</span>
					</div>
					<div className="reminder-arrow">
						<FontAwesomeIcon icon={faChevronRight} />
					</div>
				</div>
			))}
		</div>
	</>;
}
