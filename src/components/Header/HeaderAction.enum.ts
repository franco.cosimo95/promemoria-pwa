export enum HeaderAction {
  Add = 'Add',
  Settings = 'Settings',
  Delete = 'Delete',
}
