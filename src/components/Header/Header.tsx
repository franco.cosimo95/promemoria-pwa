import React from 'react';
import {HeaderAction} from './HeaderAction.enum';
import {Link} from 'react-router-dom';
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {faChevronLeft, faCog, faPlus, faQuestion, faTrash} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

interface HeaderPrev {
	title: string;
	link: string;
}

interface HeaderProps {
	title: string;
	actions?: HeaderAction[];
	onActionClick?: (action: HeaderAction) => void;
	prev?: HeaderPrev;
}

function Header(props: HeaderProps) {

	const renderAction = (action: HeaderAction) => {
		let icon: IconProp;
		switch (action) {
			case HeaderAction.Add:
				icon = faPlus;
				break;
			case HeaderAction.Settings:
				icon = faCog;
				break;
			case HeaderAction.Delete:
				icon = faTrash;
				break;
			default:
				icon = faQuestion;
				break;
		}
		return <FontAwesomeIcon
			className="icon"
			icon={icon}
			onClick={() => props.onActionClick && props.onActionClick(action)}
			key={action}
		/>;
	};

	return <div className="header">
		<span className="header-prev">{props.prev &&
			<Link to={props.prev.link}>
				<FontAwesomeIcon className="icon" icon={faChevronLeft}/>
				{props.prev.title}
			</Link>}
		</span>
		<span className="header-title">{props.title}</span>
		<span className="header-actions">
			{props.actions?.map(a => renderAction(a))}
		</span>
	</div>;
}

export default Header;
